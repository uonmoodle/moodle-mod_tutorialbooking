<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page that will export data for signup sheets using the data format API.
 *
 * @package    mod_tutorialbooking
 * @copyright  2019 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\dataformat;
use core_user\fields;
use mod_tutorialbooking\event\tutorial_exported;
use mod_tutorialbooking\export;

require(dirname(dirname(__DIR__)) . '/config.php');
require_once($CFG->libdir . '/dataformatlib.php');

$id = required_param('id', PARAM_INT);
$paramname = 'dataformat';
$dataformat = optional_param($paramname, '', PARAM_ALPHA);

list($course, $cm) = get_course_and_cm_from_cmid($id, 'tutorialbooking');
$context = context_module::instance($cm->id);

// Check that user should be here.
require_course_login($course, true, $cm);
require_capability('mod/tutorialbooking:export', $context);

// Setup the page.
$pageurl = new moodle_url('/mod/tutorialbooking/export.php');
$pageparams = ['id' => $id];
$PAGE->set_url($pageurl, $pageparams);
$PAGE->set_context($context);
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_pagelayout('incourse');
$PAGE->set_title($course->shortname . ': '. get_string('exportlistprompt', 'mod_tutorialbooking'));

if (!empty($dataformat)) {
    // Log that the data is being exported.
    $eventdata = [
        'context' => $context,
        'objectid' => $cm->instance,
    ];
    $event = tutorial_exported::create($eventdata);
    $event->trigger();

    // Do the export.
    $data = export::getexport($cm);
    $columns = [
        'SessionTitle' => get_string('timeslottitle', 'mod_tutorialbooking'),
        'RealName' => get_string('fullname'),
    ];

    // Add in all configured identity fields.
    $allfields = fields::get_identity_fields($context);
    foreach ($allfields as $field) {
        $columns[$field] = fields::get_display_name($field);
    }

    $columns['CourseFullname'] = get_string('fullnamecourse');

    $callback = [export::class, 'format_record'];
    dataformat::download_data('signup_sheet_export', $dataformat, $columns, $data, $callback);
    exit;
}

// Let the user select the export format.
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('exportlistprompt', 'mod_tutorialbooking'));
echo $OUTPUT->download_dataformat_selector(get_string('selectformat', 'mod_tutorialbooking'), $pageurl, $paramname, $pageparams);
echo $OUTPUT->footer();

ABOUT
==========
The 'Signup sheet' module was developed by
    Ben Elis
    Neill Magill

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The signup sheet module is designed to allow teachers to create sessions that the students can signup to.

INSTALLATION
==========
The Signup sheet module follows the standard installation procedure.

1. Create folder <path to your moodle dir>/mod/tutorialbooking.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.

MOBILE SUPPORT
==========
The Moodle Mobile App 3.5 and above are supported for student facing functionality.

DEVELOPMENT NOTES
==========
To run the Moodle app behat tests (annotated with @app) will require that the
[local_moodlemobileapp plugin](https://moodle.org/plugins/view.php?plugin=local_moodlemobileapp)
is installed in your Moodle environment.

If you do not have that plugin installed please ensure that Moodle app tests are not run during
behat if you wish to avoid failures.

<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the tutorial booking mod_tutorialbooking move slot web service.
 *
 * @package     mod_tutorialbooking
 * @category    test
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_tutorialbooking\external;

use core_external\external_api;

/**
 * Tests the tutorial booking mod_tutorialbooking move slot web service.
 *
 * @package     mod_tutorialbooking
 * @category    test
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runTestsInSeparateProcesses
 * @group mod_tutorialbooking
 * @group uon
 */
class moveslot_test extends \advanced_testcase {
    /**
     * Tests that the correct response is generated when the slow is moved 'up'
     */
    public function test_moveslot_up() {
        global $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_tutorialbooking');
        $course = self::getDataGenerator()->create_course();
        $tutorialbooking = $generator->create_instance(array('course' => $course->id));
        $slot1 = $generator->add_slot($tutorialbooking, array('spaces' => 2));
        $slot2 = $generator->add_slot($tutorialbooking, array('spaces' => 3));
        $slot3 = $generator->add_slot($tutorialbooking, array('spaces' => 5));
        $slot4 = $generator->add_slot($tutorialbooking, array('spaces' => 2));

        self::setAdminUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'tutorial' => $tutorialbooking->id,
            'slot' => $slot2->id,
            'target' => $slot4->id,
        ];
        $result = external_api::call_external_function('mod_tutorialbooking_moveslot', $args);

        $expectedresult = array(
            'success' => true,
            'where' => 'after',
        );

        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Tests that the correct response is generated when moving slots 'down'.
     */
    public function test_moveslot_down() {
        global $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_tutorialbooking');
        $course = self::getDataGenerator()->create_course();
        $tutorialbooking = $generator->create_instance(array('course' => $course->id));
        $slot1 = $generator->add_slot($tutorialbooking, array('spaces' => 2));
        $slot2 = $generator->add_slot($tutorialbooking, array('spaces' => 3));
        $slot3 = $generator->add_slot($tutorialbooking, array('spaces' => 5));
        $slot4 = $generator->add_slot($tutorialbooking, array('spaces' => 2));

        self::setAdminUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'tutorial' => $tutorialbooking->id,
            'slot' => $slot3->id,
            'target' => $slot1->id,
        ];
        $result = external_api::call_external_function('mod_tutorialbooking_moveslot', $args);

        $expectedresult = array(
            'success' => true,
            'where' => 'before',
        );

        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Tests that an exception is raised if the user does not have the correct capability.
     */
    public function test_no_capability() {
        global $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_tutorialbooking');
        $course = self::getDataGenerator()->create_course();
        $tutorialbooking = $generator->create_instance(array('course' => $course->id));
        $slot1 = $generator->add_slot($tutorialbooking, array('spaces' => 2));
        $slot2 = $generator->add_slot($tutorialbooking, array('spaces' => 3));

        self::setGuestUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'tutorial' => $tutorialbooking->id,
            'slot' => $slot1->id,
            'target' => $slot2->id,
        ];
        $result = external_api::call_external_function('mod_tutorialbooking_moveslot', $args);

        $this->assertTrue($result['error']);
        $this->assertEquals('nopermissions', $result['exception']->errorcode);
    }

    /**
     * Tests that an exception is generated when the slots do not exist.
     */
    public function test_invalid_slot() {
        global $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_tutorialbooking');
        $course = self::getDataGenerator()->create_course();
        $tutorialbooking = $generator->create_instance(array('course' => $course->id));

        self::setAdminUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'tutorial' => $tutorialbooking->id,
            'slot' => 5,
            'target' => 6,
        ];
        $result = external_api::call_external_function('mod_tutorialbooking_moveslot', $args);

        $this->assertTrue($result['error']);
        $this->assertEquals(
            get_string('ajax_slots_not_exist', 'mod_tutorialbooking'),
            $result['exception']->message
        );
    }

    /**
     * Tests that an exception is raised when the slots are not part of the correct tutorial booking.
     */
    public function test_slot_in_different_tutorial() {
        global $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_tutorialbooking');
        $course = self::getDataGenerator()->create_course();
        $tutorialbooking1 = $generator->create_instance(array('course' => $course->id));
        $slot1 = $generator->add_slot($tutorialbooking1, array('spaces' => 2));
        $slot2 = $generator->add_slot($tutorialbooking1, array('spaces' => 3));
        $tutorialbooking2 = $generator->create_instance(array('course' => $course->id));
        $generator->add_slot($tutorialbooking2, array('spaces' => 3));
        $generator->add_slot($tutorialbooking2, array('spaces' => 3));

        self::setAdminUser();
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'tutorial' => $tutorialbooking2->id,
            'slot' => $slot1->id,
            'target' => $slot2->id,
        ];
        $result = external_api::call_external_function('mod_tutorialbooking_moveslot', $args);

        $this->assertTrue($result['error']);
        $this->assertEquals(
            get_string('ajax_invalid_slots', 'mod_tutorialbooking'),
            $result['exception']->message
        );
    }
}

// This file is part of the tutorial booking activity plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A javascript module that allows tutorial booking slots to
 * be moved by dragging and dropping them.
 *
 * @module     mod_tutorialbooking/move
 * @copyright  2019 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import $ from 'jquery';
import Log from 'core/log';
import * as Notification from 'core/notification';
import * as Ajax from 'core/ajax';
import SortableList from 'core/sortable_list';
import * as Str from 'core/str';
import * as Template from 'core/templates';

const SELECTORS = {
    DRAGGABLE: '.tutorial_session',
    DRAGAREA: '.tutorial_sessions',
    HANDLEELEMENT: '.tutorial_session .sectionname',
    MOVEDOWNCONTROL: '.tutorial_session .controls .movedown',
    MOVEUPCONTROL: '.tutorial_session .controls .moveup'
};

/**
 * Stores the original order of the slots.
 * @type {ElementList}
 */
let origOrder;

/**
 * Callback to remove a html element from the DOM.
 *
 * @param {HTMLElement} element
 */
const removeElement = (element) => {
    element.parentNode.removeChild(element);
};

/**
 * Sets up a tutorial booking page for drag and drop.
 *
 * @returns {undefined}
 */
export const init = () => {
    Log.debug('Setting up', 'mod_tutorialbooking/dragdrop');
    // Remove the move controls.
    Array.from(document.querySelectorAll(SELECTORS.MOVEUPCONTROL)).forEach(removeElement);
    Array.from(document.querySelectorAll(SELECTORS.MOVEDOWNCONTROL)).forEach(removeElement);

    // Add the drag handle and setup the sortable list.
    createHandles();
    let list = new SortableList(SELECTORS.DRAGAREA);
    list.getElementName = getSlotName;

    // These events are fired by the jQuery trigger method, they do not trigger native Javascript event listening
    // so we still need to use jQuery here.
    $(SELECTORS.DRAGAREA + ' ' + SELECTORS.DRAGGABLE).on(SortableList.EVENTS.DRAGSTART, startDrag)
        .on(SortableList.EVENTS.DROP, drop);
    Log.debug('Setup completed', 'mod_tutorialbooking/dragdrop');
};

/**
 * Gets the name of a slot.
 *
 * @param {Element} element
 * @returns {Promise}
 */
const getSlotName = (element) => {
    return Promise.resolve(element.attr('data-name'));
};

/**
 * Attaches a drag handler to an element.
 *
 * @param {HTMLElement} element
 * @returns {undefined}
 */
const attachHandle = async(element) => {
    let string = await Str.get_string('moveslot', 'mod_tutorialbooking', element.innerText);
    let htmlString = await Template.render('core/drag_handle', {movetitle: string});
    // Convert the string into nodes and then attach it.
    let html = document.createRange().createContextualFragment(htmlString);
    element.prepend(html);
    return;
};

/**
 * Generates the handles for the sessions.
 */
const createHandles = async() => {
    Array.from(document.querySelectorAll(SELECTORS.HANDLEELEMENT)).forEach(await attachHandle);
    return;
};

/**
 * Handles a drag start.
 *
 * @param {Event} e
 * @param {Object} info Information passed by the SortableList events
 * @returns {undefined}
 */
const startDrag = (e, info) => {
    Log.debug('Drag started', 'mod_tutorialbooking/move');
    // Remember position of the element in the beginning of dragging.
    origOrder = info.sourceList.children();
};

/**
 * Handles a drop event.
 *
 * Moves the slot to it's new position.
 *
 * @param {Event} e
 * @param {Object} info Information passed by the SortableList events
 * @returns {undefined}
 */
const drop = (e, info) => {
    Log.debug('Dropped element', 'mod_tutorialbooking/move');
    let newIndex = info.targetList.children().index(info.element);

    // Find the element being dragged.
    let tutorial = e.target.closest(SELECTORS.DRAGAREA);

    // Make the AJAX call to move the slot.
    let slot = info.element.attr('id');
    let target = origOrder.get(newIndex).getAttribute('id');
    let moveslot = {
        methodname: 'mod_tutorialbooking_moveslot',
        args: {
            tutorial: parseTutorialId(tutorial),
            slot: parseSlotId(slot),
            target: parseSlotId(target)
        }
    };
    // This is still using a jQuery Promise because the Ajax library still uses it.
    let calls = Ajax.call([moveslot]);
    calls[0].done((response) => {
        if (response.success) {
            // Move the slot.
            if (response.where === 'before') {
                let message = 'Slot ' + slot + ' moved before slot ' + target;
                Log.debug(message, 'mod_tutorialbooking/dragdrop');
            } else {
                let message2 = 'Slot ' + slot + ' moved after slot ' + target;
                Log.debug(message2, 'mod_tutorialbooking/dragdrop');
            }
        }
    }).fail(Notification.exception);
};

/**
 * Gets the id of the tutorial based on it's css id.
 *
 * They will always have a css id in the form: tutorial-<number>
 *
 * @param {HTMLElement} element
 * @returns {String} The tutorial booking id number.
 */
const parseTutorialId = (element) => {
    return element.getAttribute('id').slice(9);
};

/**
 * Gets the id of the slot based on it's css id.
 *
 * They will always have a css id in the form: slot-<number>
 *
 * @param {String} id
 * @returns {String} The slot id number.
 */
const parseSlotId = (id) => {
    return id.slice(5);
};

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_tutorialbooking\completion;

use lang_string;

/**
 * Defines the custom completion rules for the activity.
 *
 * @package    mod_tutorialbooking
 * @copyright  2022 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class custom_completion extends \core_completion\activity_custom_completion {
    /**
     * Tests if the user is signed up.
     *
     * @return int
     */
    protected function check_completionsignedup(): int {
        global $DB;
        $params = [
            'tutorialid' => $this->cm->instance,
            'userid' => $this->userid
        ];
        $issignedup = $DB->record_exists('tutorialbooking_signups', $params);
        return ($issignedup) ? COMPLETION_COMPLETE : COMPLETION_INCOMPLETE;
    }

    /**
     * @inheritDoc
     */
    public function get_state(string $rule): int {
        $this->validate_rule($rule);

        $state = COMPLETION_INCOMPLETE;

        switch ($rule) {
            case 'completionsignedup':
                $state = $this->check_completionsignedup();
                break;
        }

        return $state;
    }

    /**
     * @inheritDoc
     */
    public static function get_defined_custom_rules(): array {
        return ['completionsignedup'];
    }

    /**
     * @inheritDoc
     */
    public function get_custom_rule_descriptions(): array {
        return [
            'completionsignedup' => get_string('signuotosession', 'tutorialbooking')
        ];
    }

    /**
     * @inheritDoc
     */
    public function get_sort_order(): array {
        return [
            'completionview',
            'completionsignedup',
        ];
    }
}

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_tutorialbooking\navigation\views;

use navigation_node;
use settings_navigation;

/**
 * Class secondary_navigation_view.
 *
 * Custom implementation for a plugin.
 *
 * @package     mod_tutorialbooking
 * @category    navigation
 * @copyright   2023 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class secondary extends \core\navigation\views\secondary {
    protected function get_default_module_mapping(): array {
        return [
            self::TYPE_SETTING => [
                'modedit' => 1,
                "mod_{$this->page->activityname}_useroverrides" => 6, // Overrides are module specific.
                "mod_{$this->page->activityname}_groupoverrides" => 7,
                'roleassign' => 9.2,
                'filtermanage' => 8,
                'roleoverride' => 9,
                'rolecheck' => 9.1,
                'logreport' => 10,
                'backup' => 11,
                'restore' => 12,
                'competencybreakdown' => 13,
            ],
            self::TYPE_CUSTOM => [
                'mod_tutorialbooking_sessions' => 2,
                'mod_tutorialbooking_messages' => 3,
                'mod_tutorialbooking_export' => 4,
                'mod_tutorialbooking_index' => 5,
                'contentbank' => 14,
            ],
        ];
    }

    protected function load_module_navigation(settings_navigation $settingsnav, ?navigation_node $rootnode = null): void {
        $rootnode = $rootnode ?? $this;
        $mainnode = $settingsnav->find('modulesettings', self::TYPE_SETTING);
        $nodes = $this->get_default_module_mapping();

        if ($mainnode) {
            $page = $settingsnav->get_page();
            if (has_capability('mod/tutorialbooking:viewadminpage', $page->context)) {
                // Teachers have a different main activity page.
                $url = new \moodle_url(
                    '/mod/tutorialbooking/tutorialbooking_sessions.php',
                    [
                        'tutorialid' => $page->cm->instance,
                        'courseid' => $page->course->id
                    ]
                );
            } else {
                // The main student page.
                $url = new \moodle_url('/mod/tutorialbooking/view.php', ['id' => $page->cm->id]);
            }
            $setactive = $url->compare($page->url, URL_MATCH_EXACT);
            $node = $rootnode->add(
                get_string('modulename', $page->activityname),
                $url,
                null,
                null,
                'modulepage'
            );
            if ($setactive) {
                $node->make_active();
            }
            // Add the initial nodes.
            $nodesordered = $this->get_leaf_nodes($mainnode, $nodes);
            $this->add_ordered_nodes($nodesordered, $rootnode);

            // We have finished inserting the initial structure.
            // Populate the menu with the rest of the nodes available.
            $this->load_remaining_nodes($mainnode, $nodes, $rootnode);
        }
    }
}
